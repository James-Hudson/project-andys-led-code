/*********************************************************************
This is an example for our Monochrome OLEDs based on SSD1306 drivers
*********************************************************************/

//#include <SPI.h>
#include <Wire.h>
//#include <Adafruit_GFX.h>
#include <gfxfont.h>
#include <Adafruit_SSD1306.h>
#include "PCA9544A.h"
#include <Fonts/FreeSerifBold18pt7b.h>
#include <Fonts/FreeSerifBold12pt7b.h>
//#include <Fonts/FreeSerifBold9pt7b.h>
//#include <Fonts/FreeSerif9pt7b.h>
#include <Fonts/FreeMono9pt7b.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

void setup()   {

  Wire.begin();
  Serial.begin(9600);

// INITIALISE ALL THREE OLED DISPLAYS
// **********************************
  mux_channel(0);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();
  display.setTextColor(WHITE);
  mux_channel(1);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();
  display.setTextColor(WHITE);
  mux_channel(2);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();
  display.setTextColor(WHITE); 
}

void loop() {
// CENTRE LARGER DISPLAY DRIVE (128W x 64H)
// ****************************************
  mux_channel(0);
  display.clearDisplay();

  display.drawPixel(0, 0, WHITE); //top left
  display.drawPixel(63, 0, WHITE); //top centre
  display.drawPixel(127, 0, WHITE); //top right

  display.drawPixel(0, 31, WHITE); //centre left
  display.drawPixel(63, 31, WHITE); //centre
  display.drawPixel(127, 31, WHITE); //centre right

  display.drawPixel(0, 63, WHITE); //bottom left
  display.drawPixel(63, 63, WHITE); //bottom centre
  display.drawPixel(127, 63, WHITE); //bottom right
  display.display();
  delay(1000);
  display.setFont(&FreeSerifBold18pt7b);
  display.setCursor(16,28);
  display.println("Cyrex");
  display.display();
  delay(1000);
   
  testdrawline();
  delay(1000);
 
  display.clearDisplay();
  display.setFont(&FreeSerifBold12pt7b);
  display.setCursor(0,40);
  display.println("ElectraStim");
  display.display();
  delay(1000);

// CHANNEL 1 (LEFT) DISPLAY DRIVE (64W x 48H)
// ******************************************
  mux_channel(1);
  display.clearDisplay();
  display.setFont(&FreeMono9pt7b);
  display.setCursor(50,52);
  display.print("CH1");
  display.setCursor(38,63);
  display.print("POWER");
  
  display.setFont(&FreeSerifBold18pt7b);
  display.setCursor(46,40);
  display.print("25");
  display.display();
  delay(1000);
  
  display.clearDisplay();
  display.setFont(&FreeMono9pt7b);
  display.setCursor(50,52);
  display.print("CH1");
  display.setCursor(38,63);
  display.print("POWER");
  
  display.setFont(&FreeSerifBold18pt7b);
  display.setCursor(46,40);
  display.print("36");
  display.display();
  delay(1000);

// CHANNEL 2 (RIGHT) DISPLAY DRIVE 64W x 48H)
// ******************************************
  mux_channel(2);
  display.clearDisplay();
  display.drawPixel(32, 16, WHITE); //top left
  display.drawPixel(63, 16, WHITE); //top centre
  display.drawPixel(94, 16, WHITE); //top right

  display.drawPixel(32, 40, WHITE); //centre left
  display.drawPixel(63, 40, WHITE); //centre
  display.drawPixel(94, 40, WHITE); //centre right

  display.drawPixel(32, 63, WHITE); //bottom left
  display.drawPixel(63, 63, WHITE); //bottom centre
  display.drawPixel(94, 63, WHITE); //bottom right
  display.display();
  
  display.setFont(&FreeSerifBold18pt7b);
  display.setCursor(32,40);
  display.print("88");
  display.display();
  delay(1000);

  display.setCursor(63,63);
  display.print("77");
  display.display();
  delay(1000);

}

void testdrawline() {  
  for (int16_t i=0; i<display.width(); i+=8) {
    display.drawLine(63, 31, i, display.height()-1, WHITE);
    display.display();
    delay(1);}

}

